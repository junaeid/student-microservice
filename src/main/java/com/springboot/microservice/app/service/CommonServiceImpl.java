package com.springboot.microservice.app.service;

import com.springboot.microservice.app.dto.response.AddressResponse;
import com.springboot.microservice.app.feignclient.AddressFeignClient;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class CommonServiceImpl {
    long count = 1;
    private final AddressFeignClient addressFeignClient;

    public CommonServiceImpl(AddressFeignClient addressFeignClient) {
        this.addressFeignClient = addressFeignClient;
    }

    @CircuitBreaker(
            name = "addressService",
            fallbackMethod = "fallbackGetAddressById"
    )
    public AddressResponse getAddressById(Long addressId) {
        log.info("count + " + count);
        count++;

        AddressResponse response = addressFeignClient.getById(addressId);
        return response;
    }

    public AddressResponse fallbackGetAddressById(Long addressId, Throwable th) {
        log.error("Error = " + th);
        return new AddressResponse();
    }
}
