package com.springboot.microservice.app.service.impl;

import com.springboot.microservice.app.dto.request.StudentRequest;
import com.springboot.microservice.app.dto.response.StudentResponse;
import com.springboot.microservice.app.model.Student;
import com.springboot.microservice.app.repository.StudentRepository;
import com.springboot.microservice.app.service.CommonServiceImpl;
import com.springboot.microservice.app.service.StudentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;

    private final CommonServiceImpl commonService;

    public StudentServiceImpl(
            StudentRepository studentRepository,
            CommonServiceImpl commonService
    ) {
        this.studentRepository = studentRepository;
        this.commonService = commonService;
    }

    @Override
    public StudentResponse createStudent(StudentRequest studentRequest) {
        Student student = Student.builder()
                .firstName(studentRequest.getFirstName())
                .lastName(studentRequest.getLastName())
                .email(studentRequest.getEmail())
                .addressId(studentRequest.getAddressId())
                .build();
        studentRepository.save(student);

        return studentToStudentResponse(student);
    }

    @Override
    public StudentResponse getById(Long id) {
        log.info("Inside Student Get By ID");
        Student student = studentRepository.findById(id).get();
        return studentToStudentResponse(student);
    }


    private StudentResponse studentToStudentResponse(Student student) {
        StudentResponse studentResponse = StudentResponse.builder()
                .id(student.getId())
                .firstName(student.getFirstName())
                .lastName(student.getLastName())
                .email(student.getEmail())
                //.addressResponse(getAddressById(student.getAddressId()))
                .addressResponse(commonService.getAddressById(student.getAddressId()))
                .build();
        return studentResponse;
    }

}
