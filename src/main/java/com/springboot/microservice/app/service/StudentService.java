package com.springboot.microservice.app.service;

import com.springboot.microservice.app.dto.request.StudentRequest;
import com.springboot.microservice.app.dto.response.AddressResponse;
import com.springboot.microservice.app.dto.response.StudentResponse;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;

public interface StudentService {

    StudentResponse createStudent(StudentRequest studentRequest);

    StudentResponse getById(Long id);

}
