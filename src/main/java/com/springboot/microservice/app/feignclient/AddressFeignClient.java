package com.springboot.microservice.app.feignclient;

import com.springboot.microservice.app.dto.response.AddressResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "API-GATEWAY")
public interface AddressFeignClient {

    @GetMapping("address-service/api/v1/address/{id}")
    public AddressResponse getById(@PathVariable("id") Long id);
}
