package com.springboot.microservice.app.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;


@Configuration
@EnableFeignClients("com.springboot.microservice.app.feignclient")
public class WebClientConfig {

    @Value("${address.service.url}")
    private String addressServiceUrl;

    @Bean
    public WebClient webClient() {
        WebClient webClient = WebClient.builder()
                .baseUrl(addressServiceUrl)
                .build();

        return webClient;
    }
}
