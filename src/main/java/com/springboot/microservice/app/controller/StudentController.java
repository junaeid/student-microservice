package com.springboot.microservice.app.controller;

import com.springboot.microservice.app.dto.request.StudentRequest;
import com.springboot.microservice.app.dto.response.StudentResponse;
import com.springboot.microservice.app.service.StudentService;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/api/v1/student")
@RestController
public class StudentController {

    private final StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping("{id}")
    public StudentResponse getStudentById(@PathVariable("id") Long id) {
        return studentService.getById(id);
    }

    @PostMapping("create")
    public StudentResponse createStudent(
            @RequestBody StudentRequest studentRequest
    ) {
        return studentService.createStudent(studentRequest);
    }
}
