package com.springboot.microservice.app.bootstrap;

import com.springboot.microservice.app.dto.response.StudentResponse;
import com.springboot.microservice.app.model.Student;
import com.springboot.microservice.app.repository.StudentRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class StudentDataLoader implements CommandLineRunner {

    private final StudentRepository studentRepository;

    public StudentDataLoader(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override
    public void run(String... args) throws Exception {

        Student student = Student.builder()
                .firstName("John")
                .lastName("Doe")
                .email("john.doe@email.com")
                .addressId(1L)
                .build();

        studentRepository.save(student);
    }
}
