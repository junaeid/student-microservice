package com.springboot.microservice.app.dto.response;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AddressResponse {
    private long addressId;
    private String street;
    private String city;
}
