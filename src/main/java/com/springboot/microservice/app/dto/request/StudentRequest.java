package com.springboot.microservice.app.dto.request;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StudentRequest {
    private String firstName;
    private String lastName;
    private String email;
    private Long addressId;
}
